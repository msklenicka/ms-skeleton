import time

from BaseModels import CacheModel


class ApplicationCache:
    __cache_storage = dict()

    @staticmethod
    def save_to_cache(cache: CacheModel):
        ApplicationCache.__cache_storage[cache.cache_name] = cache

    @staticmethod
    def save_to_cache(cache: CacheModel, cache_name: str):
        ApplicationCache.__cache_storage[cache_name] = cache

    @staticmethod
    def save_to_cache(cache_name: str, data):
        cache = CacheModel(cache_name=cache_name, timestamp=time.time(), data=data)
        # cache.cache_name = cache_name
        # cache.data = data
        # cache.timestamp = time.time()
        ApplicationCache.__cache_storage[cache_name] = cache

    @staticmethod
    def get_cache(cache_name: str) -> CacheModel:
        return ApplicationCache.__cache_storage[cache_name]

    @staticmethod
    def get_caches():
        return ApplicationCache.__cache_storage

    @staticmethod
    def empty_cache():
        ApplicationCache.__cache_storage.clear()

    @staticmethod
    def empty_cache(cache_name: str):
        del (ApplicationCache.__cache_storage[cache_name])

    @staticmethod
    def get_cache_timestamp(cache_name: str) -> time:
        return ApplicationCache.get_cache(cache_name).timestamp

    @staticmethod
    def cache_exists(cache_name: str) -> bool:
        if cache_name in ApplicationCache.get_caches():
            return True
        return False

from colorlog import ColoredFormatter
import logging


class LogService:

    @staticmethod
    def get_formatter():
        return ColoredFormatter(
            "%(log_color)s%(levelname)-9s%(reset)s %(message)s",
            log_colors={
                'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red',
            },
            reset=True,
            style='%')

    @staticmethod
    def get_logger(class_name):
        log = logging.getLogger(class_name)
        log.setLevel(logging.DEBUG)
        formatter = LogService.get_formatter()
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        log.addHandler(handler)
        return log

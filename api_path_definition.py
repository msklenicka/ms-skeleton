from configuration import Configuration
from BaseModels import FastApiPathModel
from starlette import status


class ApiPathDefinitions:
    api_paths = dict()
    api_paths['api_root'] = FastApiPathModel(
        path='/',
        summary='Root Endpoint',
        description='Manages requests to the root endpoint and provides application status',
        response_description='Returns application status and version number',
        tags=['Application status'],
        name='RootEndpoint')

    api_paths['docs_url'] = FastApiPathModel(
        path='/docs',
        summary='API Documentation',
        description='Provides auto-generated documentation for the API',
        response_description='API documentation page',
        tags=['Database information and management'],
        name='ApiDocumentation')

    api_paths['api_live'] = FastApiPathModel(
        path='/api/live',
        summary='Live Check',
        description='Checks if the application is live and responding',
        response_description='Confirms if live',
        tags=['Application status'],
        name='LiveCheck',
        status_code=status.HTTP_200_OK)

    api_paths['api_ready'] = FastApiPathModel(
        path='/api/ready',
        summary='Readiness Check',
        description='Checks if the application is ready to receive requests',
        response_description='Confirms if ready',
        tags=['Application status'],
        name='ReadyCheck',
        status_code=status.HTTP_200_OK)

    api_paths['api_get_configuration'] = FastApiPathModel(
        path='/api/configuration',
        summary='Get Configuration',
        description='Retrieves the current application configuration',
        response_description='Configuration details',
        tags=['Application status'],
        name='Configuration')

    api_paths['api_get_databases'] = FastApiPathModel(
        path='/api/databases',
        summary='Get Databases',
        description='Retrieves a list of all databases',
        response_description='List of databases',
        tags=['Database information and management'],
        name='AllDatabases')

    api_paths['api_get_cached_collections'] = FastApiPathModel(
        path='/api/collection-cache',
        summary='Read Cached Collections',
        description='Retrieves a list of cached collections',
        response_description='List of cached collections',
        tags=['Application status'],
        name='CollectionsCache')

    api_paths['api_get_db_collections'] = FastApiPathModel(
        path='/api/collections/{db_name}',
        summary='Get Database Collections',
        description='Retrieves collections from a specified database',
        response_description='List of collections for specified database',
        tags=['Database information and management'],
        name='DbCollections')

    api_paths['api_get_indexes'] = FastApiPathModel(
        path='/api/indexes/{db_name}/{collection_name}',
        summary='Get Collection Indexes',
        description='Retrieves a list of indexes for a specific collection',
        response_description='List of indexes in the collection',
        tags=['Database information and management'],
        name='CollectionIndexes')

    api_paths['api_create_database'] = FastApiPathModel(
        path='/api/create-database',
        summary='Create Database',
        description='Creates a new database',
        response_description='Details of the created database',
        tags=['Database information and management'],
        name='CreateDatabase')

    api_paths['api_get_latest_documents_from_all_collections'] = FastApiPathModel(
        path='/api/latest-documents-from-all-collections/{db_name}',
        summary='Get Last Document from All Collections',
        description='Retrieves the latest document from all collections in the specified database',
        response_description='Latest document from each collection',
        tags=['Data'],
        name='LatestDocumentsFromAllCollections')

    api_paths['api_get_latest_documents_from_collection'] = FastApiPathModel(
        path='/api/latest-documents_from_collection/{db_name}/{collection_name}',
        summary='Get Latest Documents from Collection',
        description='Retrieves the latest documents from a specific collection',
        response_description='Latest documents from the collection',
        tags=['Data'],
        name='LatestDocumentsFromCollection')

    api_paths['api_get_documents_from_all_collections_by_date'] = FastApiPathModel(
        path='/api/documents-from-all-collections_by_date/{db_name}',
        summary='Get Nearest Older Document',
        description='Finds the document closest to a specified date in all collections',
        response_description='Nearest older document from the database',
        tags=['Data'],
        name='NearestOlderDocumentsFromAllCollections')

    api_paths['api_get_documents_from_collection_by_date'] = FastApiPathModel(
        path='/api/documents-from-collection-by-date/{db_name}/{collection_name}',
        summary='Get Nearest Older Documents',
        description='Finds documents closest to a specified date in a specific collection',
        response_description='Nearest older documents from the collection',
        tags=['Data'],
        name='GetNearestOlderDocumentsFromCollection')

    api_paths['api_get_documents_from_collection_by_date_range'] = FastApiPathModel(
        path='/api/documents_from_collection_by_date_range/{db_name}/{collection_name}',
        summary='Get Documents in Date Range',
        description='Retrieves documents from a collection within a specified date range',
        response_description='Documents within the date range',
        tags=['Data'],
        name='CollectionDocumentsInDateRange')

    api_paths['api_get_documents'] = FastApiPathModel(
        path='/api/documents_from_collection/{db_name}/{collection_name}',
        summary='Get All Documents',
        description='Retrieves all documents from a specific collection',
        response_description='List of all documents in the collection',
        tags=['Data'],
        name='Documents')

    api_paths['api_get_documents_with_collection_prefix'] = FastApiPathModel(
        path='/api/api_get_documents_with_collection_prefix/{db_name}/{collection_name}',
        summary='Get All Documents',
        description='Retrieves all documents from a specific collection with prefix serial_',
        response_description='List of all documents in the collection',
        tags=['Data'],
        name='Documents')

    api_paths['api_get_documents_by_serial'] = FastApiPathModel(
        path='/api/api_get_documents_by_serial/{db_name}',
        summary='Get All Documents',
        description='Retrieves all documents from a specific collection with prefix serial_',
        response_description='List of all documents in the collection',
        tags=['Data'],
        name='Documents')

    api_paths['api_get_document'] = FastApiPathModel(
        path='/api/document/{db_name}/{collection_name}/{document_id}',
        summary='Get Specific Document',
        description='Retrieves a specific document from a collection using its ID',
        response_description='The requested document',
        tags=['Document'],
        name='SpecificDocument')

    api_paths['api_create_document'] = FastApiPathModel(
        path='/api/document/{db_name}/{collection_name}',
        summary='Create New Document',
        description='Creates a new document in a specified collection',
        response_description='Details of the created document',
        tags=['Document'],
        name='CreateDocument')

    api_paths['api_update_document'] = FastApiPathModel(
        path='/api/document/{db_name}/{collection_name}/{document_id}',
        summary='Update Document',
        description='Updates an existing document in a collection',
        response_description='Details of the updated document',
        tags=['Document'],
        name='UpdateDocument')

    api_paths['api_delete_document'] = FastApiPathModel(
        path='/api/document/{db_name}/{collection_name}/{document_id}',
        summary='Delete Document',
        description='Deletes a specific document from a collection',
        response_description='Confirmation of deletion',
        tags=['Document'],
        name='DeleteDocument')

    api_paths['api_get_collection_documents_by_query'] = FastApiPathModel(
        path='/api/query/{db_name}/{collection_name}',
        summary='Search documents by query',
        description="""search documents from a collection by query. Example json: 
                {
                    "query": {
                        "dateOfReadings": {
                            "$gte": "2021-08-01",
                            "$lte": "2023-09-01"
                        }
                    },
                    "sort_field": "dateOfReadings",
                    "documents_limit": 1,
                    "skip_offset": 0
                }""",
        response_description='Retrieves documents from a collection by specified query',
        tags=['Search'],
        name='SearchCollectionDocuments')

    api_paths['api_get_documents_by_query'] = FastApiPathModel(
        path='/api/query/{db_name}',
        summary='Search documents by query for all or specified collections',
        description="""search documents from a collection by query. Example json: 
                {
                    "collections":[],
                    "query": {
                        "dateOfReadings": {
                            "$gte": "2021-08-01",
                            "$lte": "2023-09-01"
                        }
                    },
                    "sort_field": "dateOfReadings",
                    "documents_limit": 1,
                    "skip_offset": 0
                }""",
        response_description='Retrieves documents from a collection by specified query',
        tags=['Search'],
        name='SearchDocuments')

    def __init__(self, config: Configuration):
        self.config = config

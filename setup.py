from setuptools import setup, find_packages
import version

setup(
    name='mongo-connector',
    version=version.__version__,
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    author='Miloslav Sklenicka',
    author_email='miloslav.sklenicka@gmail.com',
    packages=find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    include_package_data=True,
    python_requires='>=3.12',
    install_requires=open('requirements.txt').read().splitlines(),
)

import uvicorn
from consul import Consul, Check
from log_service import LogService
from api_service import ApiService
from mongo_service import MongoService
from configuration import Configuration
from api_path_definition import ApiPathDefinitions

# logger setup
log = LogService.get_logger(__name__)
consul_client = None

# load config from env or defaults because Consul not connected
config = Configuration()
# Connection to Consul
if not config.consul_disabled:
    consul_client = Consul(host=config.consul_host, port=config.consul_port)
    log.info(f"Consul address {config.consul_host}:{config.consul_port}")


def register_service(conf: Configuration):
    log.info(f"Register service")
    check_live = Check.http(url=f'http://{conf.app_host}:{conf.app_port}{ApiPathDefinitions.api_paths["api_live"].path}',
                            interval='30s')
    consul_client.agent.service.register(name='mongo-connector',
                                         address=conf.app_host,
                                         port=conf.app_port,
                                         check=check_live)


if not config.consul_disabled:
    try:
        register_service(config)
    except Exception as e:
        log.fatal(
            f"unable to register microservice in Consul service discovery "
            f"{config.consul_host}:{config.consul_port} with error: {e}")
        exit(1)

# load configuration from consul if exists
config = Configuration(consul_client)
mongo_service = MongoService(config)

if __name__ == "__main__":
    app_instance = ApiService(log, mongo_service, config)
    app_instance.setup_routes()
    app = app_instance.app
    uvicorn.run(app, host="0.0.0.0", port=8080)

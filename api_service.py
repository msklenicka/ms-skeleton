from datetime import datetime
from logging import Logger

from fastapi import FastAPI, HTTPException, Query
from fastapi.responses import JSONResponse, Response
from pymongo.errors import ServerSelectionTimeoutError
from starlette import status

from BaseModels import DatabaseNameModel, DocumentModel, QueryModel
from api_path_definition import ApiPathDefinitions
from configuration import Configuration
from mongo_service import MongoService


class ApiService:

    def __init__(self, log: Logger, mongo_service: MongoService, config: Configuration):
        self.log = log
        self.config = config
        self.mongo_service = mongo_service
        self.app = FastAPI(title="mongo-connector",
                           version=self.config.version,
                           docs_url=ApiPathDefinitions.api_paths["docs_url"].path)

    def setup_routes(self):
        self.setup_management_routes()
        self.setup_data_routes()
        self.setup_document_routes()
        self.setup_query_routes()

    def setup_management_routes(self):
        @self.app.on_event("startup")
        async def api_startup_event():
            self.log.info(f"Application start up")
            try:
                # Load databases into cache
                await self.mongo_service.get_all_databases_with_cache()
                self.log.info("Databases are cached")
                self.log.debug(f"databases:{await self.mongo_service.get_all_databases_with_cache()}")
                # Load collection into cache
                await self.mongo_service.get_all_collections_with_cache()
                self.log.info("Collections are cached")
                self.log.debug(f"collections:{await self.mongo_service.get_all_collections_with_cache()}")
            except Exception as e:
                self.log.fatal(f"unable to start Application due to Exception {e}")
                exit(1)

        @self.app.on_event("shutdown")
        async def api_shutdown():
            self.log.info(f"Application shutting down")
            self.mongo_service.close_mongo_client()

        # @self.app.middleware("http")
        # async def api_manage_request(request: Request, call_next):
        #     self.log.info(request.url)
        #     response = await call_next(request)
        #     return response

        api_path_definition = ApiPathDefinitions.api_paths["api_root"]

        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_root():
            try:
                return JSONResponse(content={"version": self.config.version})
            except Exception as e:
                self.log.fatal(f"Unable to return version status due to error {e}")
                raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)

        api_path_definition = ApiPathDefinitions.api_paths["api_live"]

        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name,
                      status_code=api_path_definition.status_code)
        async def api_live():
            try:
                return JSONResponse(content={"status": "ok"})
            except Exception as err:
                self.log.fatal(f"Unable to return OK status due to error {err}")
                raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)

        api_path_definition = ApiPathDefinitions.api_paths["api_ready"]

        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name,
                      status_code=api_path_definition.status_code)
        async def api_ready():
            try:
                await self.mongo_service.get_server_info()
                return JSONResponse(content={"status": "ok"})
            except ServerSelectionTimeoutError as e:
                self.log.fatal(f"Unable to return OK status due to error {e}")
                raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)

        api_path_definition = ApiPathDefinitions.api_paths["api_get_configuration"]

        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_configuration():
            return JSONResponse(content=self.config.__dict__())

        api_path_definition = ApiPathDefinitions.api_paths["api_get_databases"]

        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_all_databases():
            return JSONResponse(content=(await self.mongo_service.get_all_databases_with_cache()))

        api_path_definition = ApiPathDefinitions.api_paths["api_get_cached_collections"]

        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_read_cached_collections():
            return JSONResponse(content=(await self.mongo_service.get_all_collections_with_cache()))

        api_path_definition = ApiPathDefinitions.api_paths["api_get_db_collections"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_db_collections(db_name: str):
            return JSONResponse(content=(await self.mongo_service.get_all_collections_by_database(db_name)))

        api_path_definition = ApiPathDefinitions.api_paths["api_get_indexes"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_indexes(db_name: str, collection_name: str):
            return JSONResponse(content=(await self.mongo_service.get_collection_indexes(db_name, collection_name)))

        api_path_definition = ApiPathDefinitions.api_paths["api_create_database"]
        @self.app.post(api_path_definition.path,
                       summary=api_path_definition.summary,
                       description=api_path_definition.description,
                       response_description=api_path_definition.response_description,
                       tags=api_path_definition.tags,
                       name=api_path_definition.name)
        async def api_create_database(database: DatabaseNameModel):
            if await self.mongo_service.create_database(database):
                return JSONResponse(content={"status": "ok", "message": "Document updated successfully."})
            else:
                raise HTTPException(status_code=409, detail=f"Database {database.name} exists.")

    def setup_data_routes(self):
        api_path_definition = ApiPathDefinitions.api_paths["api_get_latest_documents_from_all_collections"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_latest_documents_from_all_collections(db_name: str,
                                                                documents_limit: int = Query(1),
                                                                skip_offset: int = Query(0),
                                                                sort_field: str = Query(
                                                                 self.config.mongodb_default_sorting_field)):
            data = await self.mongo_service.get_latest_documents_from_all_collections(db_name, documents_limit,
                                                                                      skip_offset, sort_field)
            return Response(content=data.json(), media_type='application/json')

        api_path_definition = ApiPathDefinitions.api_paths["api_get_latest_documents_from_collection"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_latest_documents_from_collection(db_name: str, collection_name: str,
                                                         documents_limit: int = Query(1),
                                                         skip_offset: int = Query(0),
                                                         sort_field: str = Query(
                                                             self.config.mongodb_default_sorting_field)):
            data = await self.mongo_service.get_latest_documents_from_collection(db_name, collection_name,
                                                                                 sort_field,
                                                                                 documents_limit, skip_offset)
            return Response(content=data.json(), media_type='application/json')

        api_path_definition = ApiPathDefinitions.api_paths["api_get_documents_from_all_collections_by_date"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_documents_from_all_collections_by_date(db_name: str, date: str = Query(...),
                                                        documents_limit: int = Query(1),
                                                        skip_offset: int = Query(0),
                                                        sort_field: str = Query(self.config.mongodb_default_sorting_field)):
            try:
                target_datetime = datetime.fromisoformat(date)
            except ValueError:
                raise HTTPException(status_code=400,
                                    detail="Invalid date format. Use YYYY-MM-DDTHH:MM:SS.ffffff format.")
            data = await self.mongo_service.get_collections_documents_by_date(db_name, sort_field, target_datetime,
                                                                           documents_limit, skip_offset)
            return Response(content=data.json(), media_type='application/json')

        api_path_definition = ApiPathDefinitions.api_paths["api_get_documents_from_collection_by_date"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_documents_from_collection_by_date(db_name: str, collection_name: str, date: str = Query(...),
                                                  documents_limit: int = Query(1),
                                                  skip_offset: int = Query(0),
                                                  sort_field: str = Query(self.config.mongodb_default_sorting_field)):
            try:
                target_datetime = datetime.fromisoformat(date)
            except ValueError:
                raise HTTPException(status_code=400,
                                    detail="Invalid date format. Use YYYY-MM-DDTHH:MM:SS.ffffff format.")
            data = await self.mongo_service.get_collection_documents_by_date(db_name, collection_name, sort_field,
                                                                          target_datetime, documents_limit,
                                                                          skip_offset)

            return Response(content=data.json(), media_type='application/json')



        api_path_definition = ApiPathDefinitions.api_paths["api_get_documents_from_collection_by_date_range"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_documents_from_collection_by_date_range(db_name: str,
                                                  collection_name: str,
                                                  from_date: str = Query(...),
                                                  to_date: str = Query(...),
                                                  documents_limit: int = Query(1),
                                                  skip_offset: int = Query(0),
                                                  datetime_field_name: str = Query(
                                                      self.config.mongodb_default_sorting_field)):
            try:
                from_datetime = datetime.fromisoformat(from_date)
                to_datetime = datetime.fromisoformat(to_date)
            except ValueError:
                raise HTTPException(status_code=400,
                                    detail="Invalid date format. Use YYYY-MM-DDTHH:MM:SS.ffffff format.")
            data = await self.mongo_service.get_documents_by_date_range(db_name, collection_name, datetime_field_name,
                                                                        from_datetime,
                                                                        to_datetime, documents_limit, skip_offset)
            if not data:
                raise HTTPException(status_code=404, detail="No documents found within the provided date range.")
            return Response(content=data.json(), media_type='application/json')

        api_path_definition = ApiPathDefinitions.api_paths["api_get_documents"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_documents(db_name: str, collection_name: str, documents_limit: int = Query(100),
                                    skip_offset: int = Query(0),
                                    sort_field: str = Query(self.config.mongodb_default_sorting_field)):
            data = await self.mongo_service.get_documents_data_only(db_name, collection_name, sort_field, documents_limit,
                                                          skip_offset)

            return JSONResponse(content=data, media_type='application/json')


        api_path_definition = ApiPathDefinitions.api_paths["api_get_documents_with_collection_prefix"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_documents_with_collection_prefix(db_name: str, collection_name: str, documents_limit: int = Query(100),
                                    skip_offset: int = Query(0),
                                    sort_field: str = Query(self.config.mongodb_default_sorting_field)):
            collection_name = 'serial_' + collection_name
            data = await self.mongo_service.get_documents_data_only(db_name, collection_name, sort_field, documents_limit,
                                                          skip_offset)

            return JSONResponse(content=data, media_type='application/json')

        api_path_definition = ApiPathDefinitions.api_paths["api_get_documents_by_serial"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_documents_with_collection_prefix(db_name: str, ID: str, documents_limit: int = Query(100),
                                    skip_offset: int = Query(0),
                                    sort_field: str = Query(self.config.mongodb_default_sorting_field)):
            ID = 'serial_' + ID
            data = await self.mongo_service.get_documents_data_only(db_name, ID, sort_field, documents_limit,
                                                          skip_offset)

            return JSONResponse(content=data, media_type='application/json')





    def setup_document_routes(self):
        api_path_definition = ApiPathDefinitions.api_paths["api_get_document"]
        @self.app.get(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_get_document(db_name: str, collection_name: str, document_id: str):
            data = await self.mongo_service.get_document(db_name, collection_name, document_id)
            return Response(content=data.json(), media_type='application/json')

        api_path_definition = ApiPathDefinitions.api_paths["api_create_document"]
        @self.app.post(api_path_definition.path,
                       summary=api_path_definition.summary,
                       description=api_path_definition.description,
                       response_description=api_path_definition.response_description,
                       tags=api_path_definition.tags,
                       name=api_path_definition.name)
        async def api_create_document(db_name: str, collection_name: str, document: DocumentModel,
                                      indexed_field: str = Query(self.config.mongodb_default_sorting_field)):
            result = await self.mongo_service.post_document(db_name, collection_name, document, indexed_field,
                                                            indexed_field)
            if result:
                return await api_get_document(db_name, collection_name, result)
            raise HTTPException(status_code=404, detail="unable to save")

        api_path_definition = ApiPathDefinitions.api_paths["api_update_document"]
        @self.app.put(api_path_definition.path,
                      summary=api_path_definition.summary,
                      description=api_path_definition.description,
                      response_description=api_path_definition.response_description,
                      tags=api_path_definition.tags,
                      name=api_path_definition.name)
        async def api_update_document(db_name: str, collection_name: str, document_id: str, document: DocumentModel):
            result = await self.mongo_service.put_document(db_name, collection_name, document_id, document)
            if result.modified_count == 0:
                raise HTTPException(status_code=404, detail="Document not found.")
            return JSONResponse(content={"status": "ok", "message": "Document updated successfully."})

        api_path_definition = ApiPathDefinitions.api_paths["api_delete_document"]
        @self.app.delete(api_path_definition.path,
                         summary=api_path_definition.summary,
                         description=api_path_definition.description,
                         response_description=api_path_definition.response_description,
                         tags=api_path_definition.tags,
                         name=api_path_definition.name)
        async def api_delete_document(db_name: str, collection_name: str, document_id: str):
            result = await self.mongo_service.delete_document(db_name, collection_name, document_id)
            if result.deleted_count == 0:
                raise HTTPException(status_code=404, detail="Document not found.")
            return JSONResponse(content={"status": "ok", "message": "Document deleted successfully."})

    def setup_query_routes(self):
        api_path_definition = ApiPathDefinitions.api_paths["api_get_collection_documents_by_query"]
        @self.app.post(api_path_definition.path,
                       summary=api_path_definition.summary,
                       description=api_path_definition.description,
                       response_description=api_path_definition.response_description,
                       tags=api_path_definition.tags,
                       name=api_path_definition.name)
        async def api_get_collection_documents_by_query(db_name: str, collection_name: str, search_query: QueryModel):
            data = await self.mongo_service.get_collection_documents_by_query(db_name, collection_name, search_query)
            if data is None:
                return Response(content="No data found",
                                status_code=404,
                                media_type='application/json')
            return Response(content=data.json(), media_type='application/json')

        api_path_definition = ApiPathDefinitions.api_paths["api_get_documents_by_query"]
        @self.app.post(api_path_definition.path,
                       summary=api_path_definition.summary,
                       description=api_path_definition.description,
                       response_description=api_path_definition.response_description,
                       tags=api_path_definition.tags,
                       name=api_path_definition.name)
        async def api_get_documents_by_query(db_name: str, search_query: QueryModel):
            self.log.warning(search_query)
            self.log.warning("search_query")
            data = await self.mongo_service.get_documents_by_query(db_name, search_query)
            if data is None:
                return Response(content="No data found",
                                status_code=404,
                                media_type='application/json')
            return Response(content=data.json(), media_type='application/json')

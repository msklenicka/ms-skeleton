from fastapi.testclient import TestClient
from pymongo import MongoClient
from bson.objectid import ObjectId
from monapi import app, client

client = MongoClient("mongodb://192.168.1.195:27017/")
test_db_name = "test_db"
test_collection_name = "test_collection"


def setup_module(module):
    # před spuštěním modulu vytvoříme testovací databázi a kolekci
    db = client[test_db_name]
    db.create_collection(test_collection_name)


def teardown_module(module):
    # po dokončení modulu odstraníme testovací databázi a kolekci
    client.drop_database(test_db_name)


def test_read_databases():
    client = TestClient(app)
    response = client.get("/api/databases")
    assert response.status_code == 200
    assert "test_db" in response.json()


def test_create_item():
    client = TestClient(app)
    item = {"name": "test_item", "description": "this is a test item"}
    response = client.post(f"/api/{test_db_name}/{test_collection_name}/", json=item)
    assert response.status_code == 200
    item_id = response.json()["_id"]
    assert ObjectId.is_valid(item_id)


def test_read_item():
    client = TestClient(app)
    item = {"name": "test_item", "description": "this is a test item"}
    response = client.post(f"/api/{test_db_name}/{test_collection_name}/", json=item)
    item_id = response.json()["_id"]
    response = client.get(f"/api/{test_db_name}/{test_collection_name}/{item_id}")
    assert response.status_code == 200
    assert response.json()["name"] == "test_item"


def test_update_item():
    client = TestClient(app)
    item = {"name": "test_item", "description": "this is a test item"}
    response = client.post(f"/api/{test_db_name}/{test_collection_name}/", json=item)
    item_id = response.json()["_id"]
    new_item = {"name": "updated_test_item", "description": "this is an updated test item"}
    response = client.put(f"/api/{test_db_name}/{test_collection_name}/{item_id}", json=new_item)
    assert response.status_code == 200
    assert response.json()["name"] == "updated_test_item"


def test_delete_item():
    client = TestClient(app)
    item = {"name": "test_item", "description": "this is a test item"}
    response = client.post(f"/api/{test_db_name}/{test_collection_name}/", json=item)
    item_id = response.json()["_id"]
    response = client.delete(f"/api/{test_db_name}/{test_collection_name}/{item_id}")
    assert response.status_code == 200
    assert response.json()["status"] == "item deleted"

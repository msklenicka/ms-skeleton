import os
import version

from consul import Consul
from log_service import LogService


class Configuration:

    def __init__(self, consul_client=None):
        self.log = LogService.get_logger(__name__)
        self.version = version.__version__
        self.consul_disabled: bool = bool(os.getenv("CONSUL_DISABLED", False))
        self.consul_host: str = os.getenv("CONSUL_HOST", "consul")
        self.consul_port: int = int(os.getenv("CONSUL_PORT", 8500))
        self.consul_client: Consul = consul_client
        self.app_host: str = self.get_config(self.consul_client, "APP_HOST", os.getenv("APP_HOST", "mongo-connector"))
        self.app_port: int = int(self.get_config(self.consul_client, "APP_PORT", os.getenv("APP_PORT", 80)))
        # TODO: Zmenit cache a exp. limit aby sel menit pres API
        self.documents_limit_default: int = int(self.get_config(self.consul_client, "DOCUMENTS_LIMIT_DEFAULT",
                                                                os.getenv("DOCUMENTS_LIMIT_DEFAULT", 1000)))
        self.cache_expiration_time: int = int(self.get_config(self.consul_client, "CACHE_EXPIRATION_TIME",
                                                              os.getenv("CACHE_EXPIRATION_TIME", 300)))
        self.mongodb_host = self.get_config(self.consul_client, "MONGODB_HOST", os.getenv("MONGODB_HOST", "mongodb"))
        self.mongodb_port = int(self.get_config(self.consul_client,
                                                "MONGODB_PORT",
                                                os.getenv("MONGODB_PORT", "27017")))
        self.mongodb_password = self.get_config(self.consul_client,
                                                "MONGODB_PASSWORD",
                                                os.getenv("MONGODB_PASSWORD", ""))
        self.mongodb_user = self.get_config(self.consul_client, "MONGODB_USER", os.getenv("MONGODB_USER", ""))
        self.mongodb_default_sorting_field = self.get_config(self.consul_client,
                                                             "MONGODB_DEFAULT_SORTING_FIELD",
                                                             os.getenv("MONGODB_DEFAULT_SORTING_FIELD",
                                                                       "dateOfReadings"))

    def get_config(self, consul_client, key, default=None):
        if self.consul_disabled or not consul_client:
            return default
        index, data = consul_client.kv.get(key)
        if data is None:
            return default
        else:
            return data['Value'].decode()

    def __dict__(self):
        return {
            'CONSUL_DISABLED': self.consul_disabled,
            'CONSUL_HOST': self.consul_host,
            'CONSUL_PORT': self.consul_port,
            'CONSUL_CLIENT': self.consul_client,
            'APP_HOST': self.app_host,
            'APP_PORT': self.app_port,
            'DOCUMENTS_LIMIT_DEFAULT': self.documents_limit_default,
            'CACHE_EXPIRATION_TIME': self.cache_expiration_time,
            'MONGODB_HOST': self.mongodb_host,
            'MONGODB_PORT': self.mongodb_port,
            'MONGODB_PASSWORD': self.mongodb_password,
            'MONGODB_USER': self.mongodb_user,
            'MONGODB_DEFAULT_SORTING_FIELD': self.mongodb_default_sorting_field
        }

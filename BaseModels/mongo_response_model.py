from datetime import datetime
from typing import List

from pydantic import BaseModel
from BaseModels import MongoResponseStatisticsModel, MongoResponseDataModel


class MongoResponseModel(BaseModel):
    statistics: MongoResponseStatisticsModel = MongoResponseStatisticsModel()
    data: List[MongoResponseDataModel] = []

    def set_mongo_collection_documents_with_statistics(self, database_name: str, collection_name: str,
                                                       documents, documents_limit: int = None,
                                                       skip_offset: int = None, sort_field_name: str = None):
        # update global statistics
        self.update_statistics(database_name, collection_name, documents, documents_limit=documents_limit,
                               skip_offset=skip_offset, sort_field_name=sort_field_name)
        # update collection statistics and set data
        statistics: MongoResponseStatisticsModel = MongoResponseStatisticsModel(timestamp=datetime.now().timestamp(),
                                                                                all_collection_count=1,
                                                                                collection_with_data_count=1,
                                                                                database_name=database_name,
                                                                                documents_count=int(len(documents)),
                                                                                documents_limit=documents_limit,
                                                                                skip_offset=skip_offset,
                                                                                sort_field_name=sort_field_name)
        mongo_response_data: MongoResponseDataModel = MongoResponseDataModel(statistics=statistics,
                                                                             collection=collection_name,
                                                                             data=documents)
        self.data.append(mongo_response_data)

    def join_mongo_response(self, mongo_response_model: 'MongoResponseModel', database_name: str = None, collection_name: str = None,
             all_collection_count: int = None, documents_limit: int = None, skip_offset: int = None,
             sort_field_name: str = None):
        self.update_statistics(database_name,collection_name,None,all_collection_count,
                               documents_limit=documents_limit,skip_offset=skip_offset, sort_field_name=sort_field_name)
        self.data.extend(mongo_response_model.data)
        self.statistics.collection_with_data_count = len(self.data)
        self.statistics.document_count = sum(len(d.data) for d in self.data)

    def update_statistics(self, database_name, collection_name, documents, all_collection_count=None,
                          documents_limit=None, skip_offset=None, sort_field_name=None):
        if not self.statistics:
            self.statistics = MongoResponseStatisticsModel()
        self.statistics.timestamp = datetime.now().timestamp()
        self.statistics.document_limit = documents_limit
        self.statistics.document_offset = skip_offset
        self.statistics.sort_field_name = sort_field_name
        if collection_name and documents:
            self.statistics.document_count += len(documents)
            self.statistics.collection_with_data_count += 1
        if all_collection_count:
            self.statistics.all_collection_count = all_collection_count
        self.statistics.database_name = database_name

    def set_collection_documents(self, collection_name, documents):
        self.data[collection_name].data = documents

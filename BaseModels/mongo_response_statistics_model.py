from datetime import datetime
from pydantic import BaseModel, Field


class MongoResponseStatisticsModel(BaseModel):

    class Config:
        allow_population_by_field_name = True

    _TIMESTAMP_KEY = 'timestamp'
    _DATABASE_NAME_KEY = 'databaseName'
    _DATABASE_COLLECTION_COUNT_KEY = 'allCollectionCount'
    _COLLECTION_WITH_DATA_COUNT_KEY = 'collectionWithDataCount'
    _DOCUMENT_COUNT_KEY = 'documentCount'
    _DOCUMENT_LIMIT_KEY = 'documentLimit'
    _DOCUMENT_OFFSET_KEY = 'documentOffset'
    _SORT_FIELD_NAME_KEY = 'sortFieldName'

    timestamp: datetime = Field(None, alias=_TIMESTAMP_KEY)
    all_collection_count: int = Field(0, alias=_DATABASE_COLLECTION_COUNT_KEY)
    collection_with_data_count: int = Field(0, alias=_COLLECTION_WITH_DATA_COUNT_KEY)
    database_name: str = Field(None, alias=_DATABASE_NAME_KEY)
    document_count: int = Field(0, alias=_DOCUMENT_COUNT_KEY)
    document_limit: int = Field(None, alias=_DOCUMENT_LIMIT_KEY)
    document_offset: int = Field(None, alias=_DOCUMENT_OFFSET_KEY)
    sort_field_name: str = Field(None, alias=_SORT_FIELD_NAME_KEY)

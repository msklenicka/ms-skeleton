from pydantic import BaseModel
from typing import List


class QueryModel(BaseModel):
    collections: List[str]
    query: dict
    sort_field: str
    documents_limit: int = 1
    skip_offset: int = 0

from pydantic import BaseModel


class RecordModel(BaseModel):
    driverItemId: int
    value: str

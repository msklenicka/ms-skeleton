from typing import Any

from pydantic import BaseModel


class CacheModel(BaseModel):
    cache_name: str
    timestamp: float
    data: Any

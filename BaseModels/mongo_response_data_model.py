from typing import Any, List
from pydantic import BaseModel
from BaseModels import MongoResponseStatisticsModel


class MongoResponseDataModel(BaseModel):

    collection: str
    statistics: MongoResponseStatisticsModel
    data: List[Any]

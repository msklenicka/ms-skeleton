from typing import Any, Optional, List, Set, Dict
from pydantic import BaseModel


class FastApiPathModel(BaseModel):
    path: str
    response_model: Optional[Any] = None
    status_code: Optional[int] = None
    tags: Optional[List[str]] = None
    dependencies: Optional[List[str]] = None
    summary: Optional[str] = None
    description: Optional[str] = None
    response_description: str = "Successful Response"
    responses: Optional[Dict[str, Any]] = None
    deprecated: Optional[bool] = None
    operation_id: Optional[str] = None
    response_model_include: Optional[Set[str]] = None
    response_model_exclude: Optional[Set[str]] = None
    response_model_by_alias: bool = True
    response_model_exclude_unset: bool = False
    response_model_exclude_defaults: bool = False
    response_model_exclude_none: bool = False
    include_in_schema: bool = True
    name: Optional[str] = None
    callbacks: Optional[List[str]] = None
    openapi_extra: Optional[Dict[str, Any]] = None
    generate_unique_id_function: Optional[str] = None

from datetime import datetime
from typing import List
from pydantic import BaseModel

from BaseModels import RecordModel


class DocumentModel(BaseModel):
    driverId: int
    dateOfReadings: datetime
    harvesterLicense: str
    harvesterSerialNumber: str
    records: List[RecordModel]

from pydantic import BaseModel


class DatabaseNameModel(BaseModel):
    name: str

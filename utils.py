from datetime import datetime
import re


class Utils:

    @staticmethod
    def custom_datetime_serializer(obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        raise TypeError(f"{type(obj)} is not serializable")



    @staticmethod
    def document_limit_max_value_sanitization(documents_limit: int, document_limit_default: int):
        if documents_limit > document_limit_default:
            return document_limit_default
        else:
            return documents_limit

    @staticmethod
    def convert_string_to_datetime(query):
        # Regulární výraz pro datum a čas (např. 2021-01-01T12:00:00)
        datetime_pattern = re.compile(r'^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$')

        for key, value in query.items():
            if isinstance(value, dict):
                for sub_key, sub_value in value.items():
                    if isinstance(sub_value, str) and datetime_pattern.match(sub_value):
                        query[key][sub_key] = datetime.fromisoformat(sub_value)
            elif isinstance(value, str) and datetime_pattern.match(value):
                query[key] = datetime.fromisoformat(value)

        return query

FROM python:3.12.2-alpine3.19

# nastavit pracovní adresář
WORKDIR /app

# nainstalovat závislosti
COPY requirements.txt requirements.txt
RUN apk --no-cache add curl && pip install --no-cache-dir -r requirements.txt

# zkopírovat zdrojové soubory do kontejneru
COPY . /app

# definovat proměnné prostředí
ENV MONGODB_HOST localhost
ENV MONGODB_PORT 27017
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

EXPOSE 8080

# spustit aplikaci
CMD ["python", "mongo-connector.py"]
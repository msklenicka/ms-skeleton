import json
import time
import motor
import motor.motor_asyncio

from utils import Utils
from bson import ObjectId
from datetime import datetime
from bson.errors import InvalidId
from log_service import LogService
from typing import Optional, List, Dict
from configuration import Configuration
from BaseModels import DatabaseNameModel, DocumentModel, QueryModel, MongoResponseModel
from application_cache import ApplicationCache


class MongoService:
    """
    This class provides methods to interact with MongoDB.

    Args:
        config (Configuration): The configuration object containing MongoDB connection settings.

    Attributes:
        mongo_client (motor.motor_asyncio.AsyncIOMotorClient): The MongoDB client.
        log (Logger): The logger instance.
        config (Configuration): The configuration object.

    """
    mongo_client = None

    log = LogService.get_logger(__name__)

    def __init__(self, config: Configuration):
        self.config = config

    def get_mongo_client(self):
        if self.mongo_client is None:
            self.log.info(f"Mongo address: "
                          f"{self.config.mongodb_user}@{self.config.mongodb_host}:{self.config.mongodb_port}")
            try:

                client_args = {
                    "host": self.config.mongodb_host,
                    "port": self.config.mongodb_port
                }

                # if self.config.mongodb_user and self.config.mongodb_password:
                #     client_args["username"] = self.config.mongodb_user
                #     client_args["password"] = self.config.mongodb_password

                self.mongo_client = motor.motor_asyncio.AsyncIOMotorClient(**client_args)
                self.log.info("Mongo is connected")
            except Exception as e:
                self.log.Error(
                    f"Unable to connect MongoDB {self.config.mongodb_host}:{self.config.mongodb_port,} due to error: {e}")
                exit(1)
        return self.mongo_client

    def close_mongo_client(self):
        if self.get_mongo_client() is not None:
            self.get_mongo_client().close()

    async def get_all_server_databases(self):
        """
        Retrieve all databases from the server.

        :return: A list of database names.
        """
        try:
            databases = await self.get_mongo_client().list_database_names()
            return databases
        except Exception as e:
            self.log.error(
                f"Unable to connect MongoDB {self.config.mongodb_host}:{self.config.mongodb_port} due to error: {e}")

    async def get_server_info(self):
        """
        Retrieves information about the MongoDB server.

        :return: A dictionary containing information about the MongoDB server.

        Example Usage:
            ```python
            server_info = await instance.get_server_info()
            print(server_info)
            ```

        The returned dictionary may contain various information about the MongoDB server, such as version, storage engine, and replication status.
        """
        server_info = await self.get_mongo_client().server_info()
        self.log.info(f"Mongo_client.server_info() : {json.dumps(server_info)}")
        return server_info

    async def get_collection_indexes(self, database_name: str, collection_name: str):
        """
        :param database_name: The name of the database.
        :param collection_name: The name of the collection.
        :return: A dictionary containing the index information for the specified collection.

        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]
        return await collection.index_information()

    async def refresh_database_cache(self):
        """
        Refreshes the database cache.

        :return: None
        """
        cache_name = "database_cache"
        self.log.debug("Refreshing Database_cache")
        ApplicationCache.save_to_cache(cache_name, (await self.get_all_server_databases()))

    async def get_all_collections_by_database(self, database_name: str):
        """
        Get all collections by database.

        :param database_name: The name of the database.
        :type database_name: str
        :return: A list of collection names.
        :rtype: list[str]
        :raises: Exception if unable to connect to MongoDB.

        """
        self.log.debug(f"Returning collections for database: {database_name}")
        try:
            collections = await self.get_mongo_client()[database_name].list_collection_names()
            self.log.debug(f"Found collections: {collections}")
            return collections
        except Exception as e:
            self.log.error(
                f"Unable to connect MongoDB {self.config.mongodb_host}:{self.config.mongodb_port} due to error: {e}")
            exit(1)

    async def get_all_collections(self):
        """
        Retrieve all collections for each database in the server.
        :return: A dictionary containing all collections for each database in the server.
        """
        server_collections = {}
        databases = await self.get_all_server_databases()

        # Kontrola, zda get_all_server_databases nevrátilo None
        if databases is None:
            return server_collections

        for db_name in databases:
            server_collections[db_name] = await self.get_all_collections_by_database(db_name)

        return server_collections

    async def get_all_databases_with_cache(self, use_cache: bool = True, refresh_cache: bool = False):
        """
        Get all databases with cache.

        :param use_cache: If True, use the cache to get the databases. Default is True.
        :param refresh_cache: If True, refresh the cache before getting the databases. Default is False.
        :return: A list of databases.
        """
        cache_name = "database_cache"
        if not use_cache:
            self.log.debug("database_cache disabled")
            return await self.get_all_server_databases()
        if refresh_cache:
            self.log.debug("request to refresh database_cache")
            await self.refresh_database_cache()
        if not ApplicationCache.cache_exists(cache_name) or (
                time.time() - ApplicationCache.get_cache_timestamp(cache_name)) > self.config.cache_expiration_time:
            self.log.debug("database_cache expired -> refreshing")
            await self.refresh_database_cache()
        return ApplicationCache.get_cache(cache_name).data

    async def get_all_collections_with_cache(self, use_cache: bool = True, refresh_cache: bool = False):
        """
        Get all collections with cache.

        :param use_cache: Whether to use cache. Default is True.
        :param refresh_cache: Whether to refresh the cache. Default is False.
        :return: List of collections.
        """
        cache_name = "collections_cache"
        if not use_cache:
            self.log.debug("collection_cache disabled")
            return await self.get_all_collections()
        if refresh_cache:
            self.log.debug("Request to refresh collection_cache")
            await self.refresh_all_collections_cache()
        if not ApplicationCache.cache_exists(cache_name) or (
                time.time() - ApplicationCache.get_cache_timestamp(cache_name)) > self.config.documents_limit_default:
            self.log.debug("collection_cache expired -> refreshing")
            await self.refresh_all_collections_cache()
        return ApplicationCache.get_cache(cache_name).data

    async def refresh_all_collections_cache(self):
        """
        Refreshes the cache for all collections.

        :return: None
        """
        cache_name = "collections_cache"
        self.log.debug("Refreshing Collection_cache")
        ApplicationCache.save_to_cache(cache_name, await self.get_all_collections())

    async def get_all_collections_by_database_with_cache(self, database: str):
        return (await self.get_all_collections_with_cache())[database]

    async def get_document(self, database_name: str, collection_name: str, document_id: str):
        """
        Retrieve a document from a specified database and collection in MongoDB.

        :param database_name: The name of the database in MongoDB.
        :param collection_name: The name of the collection in the specified database.
        :param document_id: The ID of the document to be retrieved.
        :return: A MongoResponseModel object containing the retrieved document.
        """
        db = self.get_mongo_client()[database_name]
        documents = []
        collection = db[collection_name]
        try:
            document = await collection.find_one({"_id": ObjectId(document_id)})
        except InvalidId:
            self.log.warning(f"InvalidId: {document_id}")
            return None
        document["_id"] = str(document["_id"])
        documents.append(document)
        mongo_response: MongoResponseModel = MongoResponseModel()
        mongo_response.set_mongo_collection_documents_with_statistics(database_name, collection_name, documents)
        return mongo_response

    async def get_latest_documents_from_collection(self, database_name: str, collection_name: str,
                                                   sort_field_name: str, documents_limit: int, offset: int):
        """
        :param database_name: the name of the database
        :param collection_name: the name of the collection
        :param sort_field_name: the name of the field used for sorting the documents
        :param documents_limit: the maximum number of documents to retrieve
        :param offset: the number of documents to skip before retrieving
        :return: a MongoResponseModel object containing the latest documents from the specified collection

        This method retrieves the latest documents from a specified collection in a MongoDB database. The method uses the provided database name, collection name, sort field name, documents
        * limit, and offset to perform the query. The documents are sorted in descending order based on the specified sort field name. The number of documents retrieved is limited to the specified
        * documents limit, and a certain number of documents can be skipped before retrieving the results.

        The method returns a MongoResponseModel object that contains the retrieved documents along with additional information such as the database name, collection name, documents limit, skip
        * offset, and sort field name.
        """
        db = self.get_mongo_client()[database_name]
        documents_limit: int = Utils.document_limit_max_value_sanitization(documents_limit,
                                                                           self.config.documents_limit_default)
        collection = db[collection_name]
        cursor = collection.find().sort([(sort_field_name, -1)]).skip(offset).limit(documents_limit)
        mongo_response: MongoResponseModel = MongoResponseModel()

        documents = await cursor.to_list(length=documents_limit)
        for document in documents:
            document['_id'] = str(document['_id'])

        mongo_response.set_mongo_collection_documents_with_statistics(database_name, collection_name, documents,
                                                                      documents_limit=documents_limit,
                                                                      skip_offset=offset,
                                                                      sort_field_name=sort_field_name)
        return mongo_response

    async def get_latest_documents_from_all_collections(self, database_name: str, documents_limit: int,
                                                        skip_offset: int, sort_field_name: str):
        """

        This method retrieves the latest documents from all collections in a specified database.

        :param database_name: The name of the database to retrieve documents from.
        :type database_name: str
        :param documents_limit: The maximum number of documents to retrieve from each collection.
        :type documents_limit: int
        :param skip_offset: The number of documents to skip before starting to retrieve.
        :type skip_offset: int
        :param sort_field_name: The name of the field to sort the documents by.
        :type sort_field_name: str
        :return: A MongoResponseModel object containing the retrieved documents.
        :rtype: MongoResponseModel

        """
        mongo_response: MongoResponseModel = MongoResponseModel()
        collections = await self.get_all_collections_by_database_with_cache(database_name)
        for collection_name in collections:
            response = await self.get_latest_documents_from_collection(database_name, collection_name,
                                                                       sort_field_name, documents_limit,
                                                                       skip_offset)
            mongo_response.join_mongo_response(response, database_name=database_name, collection_name=collection_name,
                                               all_collection_count=len(collections), documents_limit=documents_limit,
                                               skip_offset=skip_offset, sort_field_name=sort_field_name)

        return mongo_response

    async def get_documents(self, database_name: str, collection_name: str, sort_field_name: str,
                            documents_limit: int, offset: int):
        """
        Retrieve documents from a MongoDB collection.

        :param database_name: The name of the database containing the collection.
        :type database_name: str
        :param collection_name: The name of the collection to retrieve documents from.
        :type collection_name: str
        :param sort_field_name: The name of the field to sort the documents by.
        :type sort_field_name: str
        :param documents_limit: The maximum number of documents to retrieve.
        :type documents_limit: int
        :param offset: The number of documents to skip before retrieving.
        :type offset: int
        :return: A MongoResponseModel object containing the retrieved documents and statistics.
        :rtype: MongoResponseModel
        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]
        documents_limit: int = Utils.document_limit_max_value_sanitization(documents_limit,
                                                                           self.config.documents_limit_default)
        mongo_response: MongoResponseModel = MongoResponseModel()
        cursor = collection.find().sort(sort_field_name, -1).skip(offset).limit(documents_limit)

        documents = await cursor.to_list(length=documents_limit)
        for document in documents:
            document["_id"] = str(document["_id"])

        mongo_response.set_mongo_collection_documents_with_statistics(database_name, collection_name, documents,
                                                                      documents_limit=documents_limit,
                                                                      skip_offset=offset,
                                                                      sort_field_name=sort_field_name)
        return mongo_response


    async def get_documents_data_only(self, database_name: str, collection_name: str, sort_field_name: str,
                                      documents_limit: int, offset: int) -> List[Dict]:
        mongo_response = await self.get_documents(database_name, collection_name, sort_field_name, documents_limit, offset)

        # Extrahujeme pouze dokumenty z každého MongoResponseDataModel
        documents_only = [item.data for item in mongo_response.data]

        # Protože `documents_only` je seznam seznamů, sloučíme je do jednoho seznamu
        flat_documents_list = [doc for sublist in documents_only for doc in sublist]

        return flat_documents_list


    async def get_collection_documents_by_date(self, database_name: str, collection_name: str,
                                               datetime_field_name: str, date_time: datetime, documents_limit: int,
                                               offset: int):
        """
        :param database_name: The name of the database.
        :param collection_name: The name of the collection.
        :param datetime_field_name: The name of the field that contains the datetime value.
        :param date_time: The specific datetime value to query documents by.
        :param documents_limit: The maximum number of documents to retrieve.
        :param offset: The number of documents to skip before retrieving.
        :return: A MongoResponseModel object containing the collection documents and statistics.

        This method queries the specified collection in the given database and retrieves a list of documents that have datetime values less than or equal to the specified date_time. The retrieved
        * documents are sorted in descending order based on the datetime field. The number of documents retrieved is limited by documents_limit and the result is offset by the specified offset
        * value.

        Note:
        - The `_id` field of each retrieved document is converted to a string.
        - The documents_limit value is sanitized to ensure it is not greater than the maximum allowed value specified in the config.
        - The statistics of the collection documents, including the database name, collection name, documents limit, skip offset, and sort field name, are set in the returned MongoResponseModel
        * object.
        - If no documents are found, an empty list will be returned.

        Example usage:
        ```
        response = await get_collection_documents_by_date('mydb', 'mycollection', 'timestamp', datetime.now(), 10, 0)
        if response:
            for doc in response.documents:
                print(doc)
        ```
        """
        db = self.get_mongo_client()[database_name]
        documents_limit: int = Utils.document_limit_max_value_sanitization(documents_limit,
                                                                           self.config.documents_limit_default)
        mongo_response: MongoResponseModel = MongoResponseModel()
        collection = db[collection_name]
        cursor = collection.find({
            datetime_field_name: {"$lte": date_time}
        }).sort(datetime_field_name, -1).skip(offset).limit(documents_limit)
        documents = await cursor.to_list(length=documents_limit)
        if documents:
            for document in documents:
                document['_id'] = str(document['_id'])
        mongo_response.set_mongo_collection_documents_with_statistics(database_name, collection_name, documents,
                                                                      documents_limit=documents_limit,
                                                                      skip_offset=offset,
                                                                      sort_field_name=datetime_field_name)
        return mongo_response

    async def get_collections_documents_by_date(self, database_name: str, datetime_field_name: str,
                                                date_time: datetime, documents_limit: int, offset: int):
        """
        :param database_name: The name of the database to retrieve collections from.
        :type database_name: str
        :param datetime_field_name: The name of the field in the collection documents that represents datetime.
        :type datetime_field_name: str
        :param date_time: The datetime to filter the documents by.
        :type date_time: datetime
        :param documents_limit: The maximum number of documents to retrieve for each collection.
        :type documents_limit: int
        :param offset: The offset to skip documents in each collection.
        :type offset: int
        :return: A MongoResponseModel object containing the results of querying collections for documents with the given datetime.
        :rtype: MongoResponseModel

        """
        mongo_response: MongoResponseModel = MongoResponseModel()
        collections = await self.get_all_collections_by_database_with_cache(database_name)
        for collection_name in collections:
            resp = await self.get_collection_documents_by_date(database_name, collection_name, datetime_field_name,
                                                               date_time, documents_limit, offset)
            mongo_response.join_mongo_response(resp, database_name=database_name, collection_name=collection_name,
                                               all_collection_count=len(collections), documents_limit=documents_limit,
                                               skip_offset=offset, sort_field_name=datetime_field_name)
        return mongo_response

    async def get_documents_by_date_range(self, database_name: str, collection_name: str, datetime_field_name: str,
                                          from_datetime: datetime, to_datetime: datetime, documents_limit: int,
                                          offset: int):
        """
        :param database_name: The name of the database to retrieve documents from.
        :param collection_name: The name of the collection within the database to retrieve documents from.
        :param datetime_field_name: The name of the field containing the datetime value.
        :param from_datetime: The starting datetime value.
        :param to_datetime: The ending datetime value.
        :param documents_limit: The maximum number of documents to retrieve.
        :param offset: The number of documents to skip before retrieving.
        :return: A MongoResponseModel object containing the retrieved documents and statistics.
        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]
        documents_limit: int = Utils.document_limit_max_value_sanitization(documents_limit,
                                                                           self.config.documents_limit_default)
        cursor = (collection.find({datetime_field_name: {"$gte": from_datetime, "$lte": to_datetime}})
                  .sort(datetime_field_name, -1).skip(offset).limit(documents_limit))
        documents = await cursor.to_list(length=documents_limit)
        if not documents:
            return None
        for document in documents:
            document['_id'] = str(document['_id'])
        mongo_response: MongoResponseModel = MongoResponseModel()
        mongo_response.set_mongo_collection_documents_with_statistics(database_name, collection_name, documents,
                                                                      documents_limit=documents_limit,
                                                                      skip_offset=offset,
                                                                      sort_field_name=datetime_field_name)
        return mongo_response

    async def get_documents_by_date(self, database_name: str, collection_name: str, datetime_field_name: str,
                                    from_datetime: Optional[datetime], to_datetime: Optional[datetime],
                                    documents_limit: int, offset: int, single_date_query: bool = False):
        """
        :param database_name: The name of the database to retrieve documents from.
        :param collection_name: The name of the collection to retrieve documents from.
        :param datetime_field_name: The name of the field that stores the datetime value for filtering.
        :param from_datetime: Optional. The starting datetime for the range of documents to retrieve.
        :param to_datetime: Optional. The ending datetime for the range of documents to retrieve.
        :param documents_limit: The maximum number of documents to retrieve.
        :param offset: The number of documents to skip before retrieving the requested documents.
        :param single_date_query: Optional. If set to True, only documents with datetime <= to_datetime will be retrieved.
        :return: A list of documents that fall within the specified datetime range.

        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]
        documents_limit = Utils.document_limit_max_value_sanitization(documents_limit,
                                                                      self.config.documents_limit_default)

        query = {}
        if single_date_query:
            query[datetime_field_name] = {"$lte": to_datetime}
        else:
            query[datetime_field_name] = {"$gte": from_datetime, "$lte": to_datetime}

        cursor = (collection.find(query).sort(datetime_field_name, -1).skip(offset).limit(documents_limit))
        documents = await cursor.to_list(length=documents_limit)
        if not documents:
            return None

        for document in documents:
            document['_id'] = str(document['_id'])
        return documents

    async def post_document(self, database_name: str, collection_name: str, document: DocumentModel, indexed_field: str,
                            date_time_field: str):
        """
        :param database_name: The name of the MongoDB database.
        :type database_name: str
        :param collection_name: The name of the collection within the specified database.
        :type collection_name: str
        :param document: The document to be inserted into the collection.
        :type document: DocumentModel
        :param indexed_field: The field name to be indexed.
        :type indexed_field: str
        :param date_time_field: The field name to store the date and time of the document.
        :type date_time_field: str
        :return: The ID of the inserted document as a string.
        :rtype: str
        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]
        indexes = await collection.index_information()
        if (indexed_field + '_-1') not in indexes:
            await collection.create_index([(indexed_field, -1)])

        document_dict = document.dict()
        document_dict[date_time_field] = document.dateOfReadings.isoformat()
        result = await collection.insert_one(document_dict)
        return str(result.inserted_id)

    async def put_document(self, database_name: str, collection_name: str, document_id: str, document: DocumentModel, ):
        """
        :param database_name: The name of the MongoDB database.
        :param collection_name: The name of the collection in the database.
        :param document_id: The unique identifier of the document.
        :param document: An instance of DocumentModel, representing the document to be updated.
        :return: An awaitable that resolves to the result of the update operation.

        This method updates a document in a MongoDB collection. It takes the database name, collection name, document id, and the new document as input parameters. The method then retrieves
        * the database and collection objects using the provided names. It converts the document model to a dictionary and adds the 'dateOfReadings' field formatted as an ISO string. The method
        * then attempts to update the document in the collection based on its unique identifier using the $set operator. If an InvalidId error occurs, it logs a warning and returns None. Otherwise
        *, it returns the result of the update operation.
        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]

        document_dict = document.dict()
        document_dict['dateOfReadings'] = document.dateOfReadings.isoformat()
        try:
            result = await collection.update_one({"_id": ObjectId(document_id)}, {"$set": document_dict})
        except InvalidId:
            self.log.warning(f"InvalidId: {document_id}")
            return None
        return result

    async def delete_document(self, database_name: str, collection_name: str, document_id: str):
        """
        Delete a document from a collection in a MongoDB database.

        :param database_name: The name of the database.
        :param collection_name: The name of the collection.
        :param document_id: The ID of the document to be deleted.
        :return: The delete result (an instance of pymongo.results.DeleteResult) if successful, or None if the document
                 could not be found or an InvalidId error occurred.
        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]
        try:
            delete_result = await collection.delete_one({"_id": ObjectId(document_id)})
        except InvalidId:
            self.log.warning(f"InvalidId: {document_id}")
            return None
        return delete_result

    async def create_database(self, database_name: DatabaseNameModel):
        """
        Create a new database.

        :param database_name: The name of the database.
        :type database_name: DatabaseNameModel
        :return: True if the database was successfully created, False if it already exists.
        :rtype: bool
        """
        if database_name.name not in await self.get_mongo_client().list_database_names():
            collection = self.get_mongo_client()[database_name.name]["database_information"]
            await collection.insert_one({"creation date": datetime.fromtimestamp(time.time()),
                                         "created_by": "mongo-connector API"})
            await self.get_mongo_client()[database_name.name].command("ping")
            return True
        else:
            return False

    async def get_collection_documents_by_query(self, database_name: str, collection_name: str, query: QueryModel):
        """
        Retrieves documents from a MongoDB collection based on the given query parameters.

        :param database_name: Name of the MongoDB database.
        :param collection_name: Name of the collection within the database.
        :param query: QueryModel object containing the query parameters.
        :return: A MongoResponseModel object containing the retrieved documents and statistics.
        """
        db = self.get_mongo_client()[database_name]
        collection = db[collection_name]
        documents_limit: int = Utils.document_limit_max_value_sanitization(query.documents_limit,
                                                                           self.config.documents_limit_default)

        converted_query = Utils.convert_string_to_datetime(query.query)
        cursor = collection.find(converted_query).sort(query.sort_field, -1).skip(query.skip_offset).limit(
            documents_limit)
        documents = await cursor.to_list(length=documents_limit)

        if not documents:
            return None

        for document in documents:
            document['_id'] = str(document['_id'])

        mongo_response: MongoResponseModel = MongoResponseModel()
        mongo_response.set_mongo_collection_documents_with_statistics(database_name, collection_name, documents,
                                                                      documents_limit=documents_limit,
                                                                      skip_offset=query.skip_offset,
                                                                      sort_field_name=query.sort_field)
        return mongo_response

    async def get_documents_by_query(self, database_name: str, query: QueryModel):
        """
        Retrieves documents from the specified database based on the given query.

        :param database_name: The name of the database.
        :param query: An instance of QueryModel representing the query criteria.
        :return: An instance of MongoResponseModel containing the retrieved documents.
        """
        self.log.warning(query)
        if query.collections:
            collections = query.collections
        else:
            collections = await self.get_all_collections_by_database_with_cache(database_name)

        mongo_response: MongoResponseModel = MongoResponseModel()

        for collection_name in collections:
            docs = await self.get_collection_documents_by_query(database_name, collection_name, query)
            mongo_response.join_mongo_response(docs, database_name, collection_name,
                                               documents_limit=query.documents_limit, skip_offset=query.skip_offset,
                                               sort_field_name=query.sort_field)
        return mongo_response
